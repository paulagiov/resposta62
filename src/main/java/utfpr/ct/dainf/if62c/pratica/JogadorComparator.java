/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author schirley
 */
public class JogadorComparator implements Comparator<Jogador>{
    private boolean a;
    private boolean b; 
    private boolean c;

    public JogadorComparator(){
        this.a = true;
        this.b = true;
        this.c = true;
    }
    
    public JogadorComparator(boolean a, boolean b, boolean c){
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    @Override
    public int compare(Jogador o1, Jogador o2) {
        if(this.a){
            if(this.b){
                if(o1.numero!=o2.numero)
                    return o1.numero-o2.numero;
                else{
                    if(this.c)
                        return o1.nome.compareTo(o2.nome);
                    else
                        return -o1.nome.compareTo(o2.nome);
                }        
            }else{
                if(o1.numero!=o2.numero)
                    return o2.numero-o1.numero;
                else{
                    if(this.c)
                        return o1.nome.compareTo(o2.nome);
                    else
                        return -o1.nome.compareTo(o2.nome);
                
            }
        }
        }else {
            if(this.c){
                if(o1.nome.compareTo(o2.nome)!=0)
                    return o1.nome.compareTo(o2.nome);
                else{
                    if(this.b)
                        return o1.numero-o2.numero;
                    else
                        return o2.numero - o1.numero;
                }        
            }else{
                if(o1.nome.compareTo(o2.nome)!=0)
                    return -o1.nome.compareTo(o2.nome);
                else{
                        if(this.b)
                            return o1.numero - o2.numero;
                        else
                            return o2.numero-o1.numero;
                    }
                }
            }
        }
    }